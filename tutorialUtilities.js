//Go through and set all youtube to hidden and set event listeners.

let allYouTube = document.querySelectorAll(".YouTube");

allYouTube.forEach((item, i) => {
  let currentLabel = item.dataset.title;
  if (!currentLabel){
    currentLabel = "Help Content";
  }
  item.setAttribute("data-before", currentLabel)
  currentLabel = undefined;
  item.addEventListener("click", ()=>{
    item.classList.toggle("YouTubeShown");
    item.innerHTML = getYouTubeEmbed(item.dataset.videoSource);
  })
});


window.addEventListener("resize",()=>{
  document.querySelectorAll(".YouTubeShown").forEach((item, i)=>{
    item.innerHTML = getYouTubeEmbed(item.dataset.videoSource);
  })
})

function getYouTubeEmbed(source){
  let vidWidth = Math.floor(window.innerWidth - 50);
  let vidHeight = Math.floor(vidWidth * 0.5625);
  return `<iframe width="${vidWidth}" height="${vidHeight}" src="https://www.youtube.com/embed/${source}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
}

//Added the checkmarks for each section.
document.querySelectorAll("section").forEach((section, index) => {
  console.log(section);
  let titleOfSection = section.children[0];

  titleOfSection.addEventListener("click", ()=>{
    section.classList.toggle("collapsed");
  })
  let checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.addEventListener("change", ()=>{
    console.log(checkbox.checked);
    if (checkbox.checked){
      section.classList.add("completed");
      section.classList.add("collapsed");
    }else{
      section.classList.remove("completed");
      section.classList.remove("collapsed");
    }
  });
  section.insertBefore(checkbox, titleOfSection);

});
